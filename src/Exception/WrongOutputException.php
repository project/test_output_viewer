<?php

namespace Drupal\test_output_viewer\Exception;

/**
 * Defines an exception to throw if test output is not correct.
 */
final class WrongOutputException extends \RuntimeException {

}
