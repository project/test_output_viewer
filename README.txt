Test Output Viewer
------------------

The module provides a way to view browser test outputs through Drupal
administrative interface.

Configuration:
Navigate to the admin/config/development/test-output/settings page and specify
browser output path.

Usage:
1. Run PHPUnit browser test from command line.
2. Navigate to admin/config/development/test-output page to check the test
   outputs.
Note that the output viewer displays outputs only for one most recent test.

PHPUnit Browser test tutorial:
https://www.drupal.org/docs/8/phpunit/phpunit-browser-test-tutorial
